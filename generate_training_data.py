#!/usr/bin/env python
import os
import xlrd
import numpy as np

filename = 'data/inbody270_data@20171013-8753_Final_ToSNU.xls'

xl_workbook = xlrd.open_workbook(filename)
sheet_names = xl_workbook.sheet_names()

xl_sheet = xl_workbook.sheet_by_index(0)

Data = []
nrows = xl_sheet.nrows
ncols = xl_sheet.ncols
for row_idx in range(2,nrows):
    element = []
    for col_idx in range(0,ncols):
       element.append(xl_sheet.cell(row_idx,col_idx))
    Data.append(element)



output_col = [14,15,16,17,18,19,20,21,22]
Input_data = []
Output_data = []

idx = 0

while idx < len(Data):
    X = []
    Y = []
    for k in range(ncols):
        if not k in output_col:
            try:
                X.append(float(Data[idx][k].value))
            except:
                if Data[idx][k].value == 'F':
                    X.append(0)
                elif Data[idx][k].value == 'M':
                    X.append(1)
                else:
                    raise NotImplementedError
        elif k == 22:
            if int(Data[idx][k].value)==86:
              Y.append(6)
            else:
              Y.append(int(Data[idx][k].value))
        else:
            Y.append(float(Data[idx][k].value))
    Output_data.append(Y)
    Input_data.append(X)
    idx = idx + 1 

import pickle
data = {}
data['input'] = np.asarray(Input_data)
data['output'] = np.asarray(Output_data)

print(data['output'].shape)
with open('data/InBody_data.pkl','wb') as f:
    pickle.dump(data,f)

