@ECHO off
SET PYTHON_PATH = D:\Hilaris\python

FOR /L %%i IN (0,1,8) DO (
  ECHO %%i
  %PYTHON_PATH %\python main.py --mode %%i
)