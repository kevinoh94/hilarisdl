Hilaris Body Impedence Deep Learning Model.

Extract python.zip
Open predict.bat and train.bat
Change PYTHON_PATH to your python extracted folder
For example
SET PYTHON_PATH = D:\Hilaris\python

For tranning
Run train.bat
Trainned model file is saved in model folder

For prediction
Change the value in data/inputimpedance.json
Run predict.bat
The result will be saved in data folder (BFM.json, FFM.json, ....)
