import pickle
import tensorflow as tf
import math
import random
import numpy as np
import argparse
import os
import json
import sys
import io
import matplotlib.pyplot as plt
from scipy import stats

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # or any {'0', '1', '2'}

try:
    to_unicode = unicode
except NameError:
    to_unicode = str


test_ratio = 0.2
valid_ratio = 0.1
train_ratio = 0.7

np.random.seed(0)

batch_size = 128
hidden_spec = [32,32,32] #[64] #[32,32,32]
max_val_tol  = 30


def data_load(args):
    if args.data == 'BCA_InBody':
        filename = 'data/data.pkl'
    elif args.data == 'InBody':
        filename = 'data/InBody_data.pkl'
    else:
        print('check the data')
        raise NotImplementedError

    with open(filename,'rb') as f:
        data = pickle.load(f)
    total_num = len(data['input'])
    train_num = math.floor(total_num * train_ratio)
    valid_num = math.floor(total_num * valid_ratio)

    random_idx = np.random.permutation(total_num)
    train_idx = random_idx[:train_num]
    valid_idx = random_idx[train_num:train_num + valid_num]
    test_idx  = random_idx[train_num+valid_num:]

    input_avg  = np.mean(data['input'],0)
    output_avg = np.mean(data['output'],0)
    output_avg[-1] = 0 ## calss label
    input_std  = np.std(data['input'],0)
    output_std = np.std(data['output'],0)
    output_std[-1] = 1
    data['input'] = (data['input'] - input_avg)/input_std

    data['output'][:,-1] = data['output'][:,-1] - 1 ## class label start from 0
    with open('data/normalize_value.pkl','wb') as f:
        pickle.dump([input_avg,output_avg,input_std,output_std],f)


    #data['output'] = (data['output'] - output_avg)/output_std

    train_data = data['input'][train_idx]
    valid_data = data['input'][valid_idx]
    test_data  = data['input'][test_idx]

    train_label = data['output'][train_idx]
    valid_label = data['output'][valid_idx]
    test_label  = data['output'][test_idx]

    

    return train_data, train_label, valid_data, valid_label, test_data, test_label, input_avg, output_avg, input_std, output_std

def make_batch(data, batch_size):

    N = data.shape[0]
    batch = []
    for i in range(math.floor(N / np.float(batch_size))):
        if data.ndim == 2:
            batch.append(data[i*batch_size:(i+1)*batch_size,:])
        else:
            batch.append(data[i*batch_size:(i+1)*batch_size])
    assert batch[0].ndim == data.ndim
    return batch

def json_load(filename):
    config = json.loads(open(filename).read())
    json_data = config['data']
    num = len(config['data'])
    data = np.zeros((num,14))
    for i in range(num):
        data[i][0] = json_data[i]['Height']
        if json_data[i]['Gender'] == 'F':
            data[i][1] = 0
        elif json_data[i]['Gender'] == 'M':
            data[i][1] = 1
        else:
            raise NotImplementedError
        data[i][2] = json_data[i]['Age']
        data[i][3] = json_data[i]['Weight']
        data[i][4] = json_data[i]['RA_20kHz_Impedance']
        data[i][5] = json_data[i]['LA_20kHz_Impedance']
        data[i][6] = json_data[i]['TR_20kHz_Impedance']
        data[i][7] = json_data[i]['RL_20kHz_Impedance']
        data[i][8] = json_data[i]['LL_20kHz_Impedance']
        data[i][9] = json_data[i]['RA_100kHz_Impedance']
        data[i][10] = json_data[i]['LA_100kHz_Impedance']
        data[i][11] = json_data[i]['TR_100kHz_Impedance']
        data[i][12] = json_data[i]['RL_100kHz_Impedance']
        data[i][13] = json_data[i]['LL_100kHz_Impedance']
    return data


def main(args):
    logname = 'log/logfile'+str(args.mode)+'.txt'
    if os.path.exists(logname):
        append_write = 'a'
    else:
        append_write = 'w'
    log_file = open(logname,append_write)

    train_data, train_label, valid_data, valid_label, test_data, test_label, input_avg, output_avg, input_std, output_std = data_load(args) 

    train_label = (train_label - output_avg)/output_std
    valid_label = (valid_label - output_avg)/output_std

    label_num = train_label.shape[1]
    input_unit = train_data.shape[1]

    '''
    label_list = []
    for i in range(train_label.shape[0]):
      if train_label[i][-1] in label_list:
        pass
      else:
         label_list.append(train_label[i][-1])
         print(label_list)
         print(len(label_list))
    '''

    if args.mode in range(train_label.shape[1]):
        if args.mode == label_num - 1:
            output_unit = 20    ##### TO SMALL NUMBER OF LABEL 21 SO WE JUST OMIT THEM
            test_label  = np.reshape(test_label[:,args.mode],(test_label.shape[0]))
            train_label = np.reshape(train_label[:,args.mode],(train_label.shape[0]))
            valid_label = np.reshape(valid_label[:,args.mode],(valid_label.shape[0]))
        else:
            output_unit = 1
            test_label  = np.reshape(test_label[:,args.mode],(test_label.shape[0],1))
            train_label = np.reshape(train_label[:,args.mode],(train_label.shape[0],1))
            valid_label = np.reshape(valid_label[:,args.mode],(valid_label.shape[0],1))

    else:
        print('mode should belong to 0~8')
        raise NotImplementedError

    train_data = make_batch(train_data,batch_size)
    train_label = make_batch(train_label,batch_size)

    x = tf.placeholder(tf.float32,[None,input_unit], name='input')
    if args.mode <8 :
        y = tf.placeholder(tf.float32,[None,output_unit],name='output')
    else:
        y = tf.placeholder(tf.int32,[None],name='output')
    h = x
    for h_unit in hidden_spec:
        h = tf.contrib.layers.fully_connected(h,h_unit,activation_fn=tf.nn.tanh)  
    #h2 = tf.contrib.layers.fully_connected(h1,hidden_unit,activation_fn=tf.nn.tanh)  
    o = tf.contrib.layers.fully_connected(h,output_unit,activation_fn=None)

    if args.mode<8:
        loss = tf.reduce_mean(tf.squared_difference(y,o))
    else:
        loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels=y,logits=o))

    train_opt = tf.train.AdamOptimizer(0.0002).minimize(loss)
    saver = tf.train.Saver(max_to_keep=99999999)
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)
    sess.run(tf.global_variables_initializer())


    if args.inference:
        saver.restore(sess,'model/model_for_mode'+str(args.mode)+'.ckpt')
        with open('data/normalize_value.pkl','rb') as f:
            data_value = pickle.load(f)

        input_avg  = data_value[0]
        output_avg = data_value[1]
        input_std  = data_value[2]
        output_std = data_value[3]

        data = json_load('data/inputimpedance.json')   ### modify this filename        
        data = (data - input_avg)/input_std        
        keys = ['TBW','Protein','Minerals','BFM','FFM','SMM','PBF', 'WHR', 'VFL']
		
        inference_batch_size = 1024
        batch_num = int(np.floor(np.size(data,0)/inference_batch_size))
        if batch_num * inference_batch_size == np.size(data,0):
            pass
        else:
            batch_num = batch_num + 1

        output_json = []
        for i in range(batch_num):
            batch_data = data[i*inference_batch_size:min((i+1)*inference_batch_size,np.size(data,0)),:]
            feed = {x:batch_data}
            pred = sess.run(o, feed_dict = feed)
            if args.mode != label_num -1:
                pred = pred * output_std[args.mode] + output_avg[args.mode]
            else:
                pred = np.argmax(pred,1)

            for j in range(len(pred) - 1):
                row_data = {}
                if args.mode != label_num -1:
                    row_data[keys[args.mode]] = np.asscalar(pred[j][0])
                else:
                    row_data[keys[args.mode]] = np.asscalar(pred[j]+1)
                output_json.append(row_data)
        filename_mode = ['TBW','Protein','Minerals','BFM','FFM','SMM','PBF','WHR','VFL']

        json_filename = 'data/'+filename_mode[args.mode]+'.json'
        with io.open(json_filename,'w',encoding='utf8') as f:
            str_ = json.dumps(output_json,indent=4,separators=(',',':'),ensure_ascii=False)
            print(str_)
            f.write(to_unicode(str_))

    else:
        Epoch = 50000
        valid_best = np.inf
        valid_not_improved = 0
        for e in range(Epoch):
             train_losses = 0
             for itr in range(len(train_data)):
                 feed = {x:train_data[itr],y:train_label[itr]}
                 train_loss , _ = sess.run([loss, train_opt], feed_dict = feed)
                 train_losses = train_losses + train_loss
             feed = {x:valid_data,y:valid_label}
             valid_loss     = sess.run(loss, feed_dict = feed)
             #print('Train epoch :'+str(e)+' loss : '+str(train_losses/len(train_data)))
             print('Valid loss  :'+str(valid_loss))
             if valid_loss < valid_best:
                 valid_best = valid_loss
                 valid_not_improved = 0
             else:
                 valid_not_improved = valid_not_improved + 1
             print('valid not improved : '+str(valid_not_improved))
             if valid_not_improved >= max_val_tol:
                 break
    
        if args.tuning == 1:
            N = test_data.shape[0]
            N = math.floor(N/2.)
            test_data = test_data[:N,:]
            test_label = test_label[:N,:]
        elif args.tuning == 2:
            pass
        else:
            N = test_data.shape[0]
            N = math.floor(N/2.)
            test_data = test_data[N:,:]
            tset_label = tets_label[N:,:]

        feed = {x:test_data}
        pred = sess.run(o, feed_dict = feed)
        if args.mode !=  label_num-1:
            pred = pred * output_std[args.mode] + output_avg[args.mode]
            test_num = test_label.shape[0]
            error_rate = np.abs((pred - test_label)/test_label * 100)
            np.set_printoptions(precision=2,suppress=True)
            print("Median: " + str(np.median(error_rate,0)))
            print("Max: " + str(np.max(error_rate,0)))
            print("Min: " + str(np.min(error_rate,0)))
            print("Third Quarter: " + str(np.percentile(error_rate,75,axis=0)))
    
            res = stats.relfreq(error_rate)
            x = res.lowerlimit + np.linspace(0, res.binsize*res.frequency.size,res.frequency.size)
            sorted_error_rate = error_rate[np.argsort(-error_rate,axis=0),0]
            with open('data/error_rate.pkl','wb') as f:
                pickle.dump([error_rate,sorted_error_rate],f)
            log_file.write('hidden spec  : '+str(hidden_spec)+'\n')
            log_file.write('error median : '+str(list(np.median(error_rate,0)))+'\n')
            log_file.write('error max    : '+str(list(np.max(error_rate,0)))+'\n')
            log_file.write('error min    : '+str(list(np.min(error_rate,0)))+'\n')
            log_file.write('error quater : '+str(np.percentile(error_rate,75,axis=0))+'\n')
            log_file.write('error top 90% : '+str(np.percentile(error_rate,90,axis=0))+'\n')
            log_file.write('error top 95% : '+str(np.percentile(error_rate,95,axis=0))+'\n')
            log_file.write('error top 5  : '+str(sorted_error_rate[0:5])+'\n')
            log_file.write('error top5 arg: '+str(np.argsort(-error_rate,axis=0)[0:5])+'\n')
            log_file.write('error mean without top1 error :'+str(np.mean(sorted_error_rate[1:]))+'\n')
            log_file.write('==================================================')
            log_file.write('\n')
            if args.show:
                fig = plt.figure()
                ax = fig.add_subplot(1,1,1)
                ax.bar(x, res.frequency, width = res.binsize)
                plt.show()    
        else:
            pred = np.argmax(pred,1)
            accuracy = np.sum(np.equal(pred,test_label)) / (test_label.shape[0] + 0.0)
            log_file.write('hidden spec  : '+str(hidden_spec)+'\n')
            log_file.write('class accuracy : '+str(accuracy)+'\n')
            log_file.write('====================================================')
            log_file.write('\n')
  
        if args.save:
            saver.save(sess,'model/model_for_mode'+str(args.mode)+'.ckpt')

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--data',type=str,default='InBody') # InBody or BCA_InBody
    parser.add_argument('--mode',type=int,default='0') # 0~8 means the independent model for each output. Forget about 404
    parser.add_argument('--tuning',type=int,default=2) # 1 : tuning -> at the test we wil use the only half test data else the entire test data
    parser.add_argument('--save',type=bool,default=True)
    parser.add_argument('--show',type=bool,default=False)
    parser.add_argument('--inference',type=bool,default=False)
    args = parser.parse_args()
    main(args)
